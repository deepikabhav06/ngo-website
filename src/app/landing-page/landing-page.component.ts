import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  @ViewChild('carouselContainer')
  carouselContainer!: ElementRef;

  constructor() { }

  ngOnInit(): void {
    this.startCarousel();
  }

  startCarousel() {
    const intervalTime = 1500; // Interval time for auto-slide (in milliseconds)
    setInterval(() => {
      this.nextSlide();
    }, intervalTime);
  }

  nextSlide() {
    const slides = this.carouselContainer.nativeElement.querySelectorAll('.carousel-slide');
    const currentSlide = this.carouselContainer.nativeElement.querySelector('.current-slide');

    if (currentSlide !== null) {
      currentSlide.classList.remove('current-slide');

      let nextSlide = currentSlide.nextElementSibling;
      if (!nextSlide) {
        nextSlide = slides[0];
      }

      nextSlide.classList.add('current-slide');
    }
  }
}

